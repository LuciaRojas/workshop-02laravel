# Workshop 02 - Laravel From The Scratch (1° Entregable)Tarea


Primero que todo se levanta  vagrant con el siguiente comando `vagrant up` 


luego se el siguiente comando `vagrant rsync-auto`  

Luego entraremos donde tenemos el proyecto y lo abrimos para trabajar 
 
![coamdo tree](01.jpg "comando tree")

# Laravel From The Scratch - 2. Routing

## Basic Routing and Views

en el archivo nos vamos donde dice `routes` lo abrimos este se encargara de las rutas que se veran en la pagina   

![coamdo tree](02.jpg "comando tree")

Luego nos mostrara los siguientes archivos:

![coamdo tree](03.jpg "comando tree")

Escogeremos el que dice `web.php`
![coamdo tree](04.jpg "comando tree")

Y nos mostrara lo siguiente:

![coamdo tree](05.jpg "comando tree")

ese mismo se encuentra en la carpeta de resources/views/welcome.blade.php el cual sera el principal que se vera 
![coamdo tree](06.jpg "comando tree")

Al crear una nueva ruta pero poniendole un nombre a donde queramos que nos rederija 
![coamdo tree](07.jpg "comando tree")
si en la url le colocamos algun nombre diferente al que se encuentra en el archvio nos dara error como en la siguiente imagen 
![coamdo tree](08.jpg "comando tree")

Pero si lo colocamos como se encuentra en la anterior imagen que le pusimos `welcome` nos mostrara la siguiente imagen
![coamdo tree](09.jpg "comando tree")

Pero si en la ruta lo dejamos solo con `Route::get('/', function () {` y en el return le quitamos lo que es `  return view('welcome');` le decimos que devuelva un string  
![coamdo tree](10.jpg "comando tree")
esto es lo que nos mostrara 

Se se crea una nueva ruta en este caso  `test`
![coamdo tree](12.jpg "comando tree")

Y ademas de eso creamos un nuevo archivo de test en el viwe 

![coamdo tree](13.jpg "comando tree")

En ese archivo se creara un h1 en el cual se pondra como prueba test Completed
![coamdo tree](14.jpg "comando tree")

Luego nos vamos al navegador y colocaremos la ruta de `/test` y en el cual nos mostrara lo que escribimos en el archivo de test
![coamdo tree](15.jpg "comando tree")

    Pass Request Data to Views

La función auxiliar request () se puede utilizar para obtener datos de cualquier solicitud GET o POST. En este episodio, aprenderemos cómo obtener datos de la cadena de consulta, pasarlos a una vista y luego codificarlos para protegerlos contra posibles ataques XSS.

colocamos ahora en la ruta el siguiente codigo lo que hara es lo siguiente
la variable $name obtendra el valor que se le esta pasando por parametro que en este caso seria por la url 
y luego lo retornara

Route::get('/test', function () {
    $name = request('name');
    return $name;
});

![coamdo tree](16.jpg "comando tree")


Ahora modificando la ruta 

Route::get('/test', function () {
    $name = request('name');
    return view('test',[
        'name' => $name
    ]);
});

y ademas de eso en el viwe de test agregamos lo siguiente 

<h1>  <?=$name; ?> </h1>
o

<h1>{!! $name !!}</h1>
para que el parametro que pasamos nos lo imprima en esa vista

![coamdo tree](17.jpg "comando tree")


ademas utilizando lo siguiente en la url podemos hacer un alert http://blog.isw811.xyz/test?name=%3Cscript%3Ealert(%27Lucia%27);%3C/script%3E

![coamdo tree](18.jpg "comando tree")

    
    
## Route Wildcards


A menudo, deberá construir una ruta que acepte un valor comodín. Por ejemplo, al ver una publicación específica, parte del URI deberá ser único. En estos casos, podemos utilizar un comodín de ruta.    




Mediante la siguiente forma mandaremos algun valor por formulario  

Route::get('/test/{post}', function ($post) {
   return $post;
});

![coamdo tree](19.jpg "comando tree")



Ahora mediante un arreglo agregaremos dos valores para dos mensaje el cual esperremos por la url lo que seria  My-firts-post o My-second-post dependiendo de cual llegue es e mensaje que devolvera y si no esta ninguno de los dos devolvera Nothing here yet


Route::get('/test/{post}', function ($post) {
  
    $posts=[
        'My-firts-post' =>'Hello, this is my first blog post!',
        'My-second-post' =>'Now I am getting the hand of this bloggin'
    ];

    return view('test',[
        'post' => $posts[$post] ?? 'Nothing here yet.'
    ]);

});


<h1>{!! $post !!}</h1>

![coamdo tree](20.jpg "comando tree")


le siguiente manera tambien sirve para controlar algun error 

Route::get('/test/{post}', function ($post) {
  
    $posts=[
        'My-firts-post' =>'Hello, this is my first blog post!',
        'My-second-post' =>'Now I am getting the hand of this bloggin'
    ];
            if(!array_key_exists($post, $posts)){
                abort(404, 'sorry that post was not found.')
            }

    return view('test',[
        'post' => $posts[$post] 
    ]);

});



## Routing to Controllers

Es bueno que podamos proporcionar un cierre para manejar la lógica de cualquier ruta, sin embargo, es posible que encuentre que para proyectos más importantes, casi siempre buscará un controlador dedicado. Aprendamos cómo en esta lección.


esto no redirijira al archivo de PostsController el cual contendra las rutas
`Route::get('/test/{post}','PostsController@show');`

tuve que cambiar un poco la ruta para que me siviera 

`Route::get('/posts/{post}','App\Http\controllers\PostsController@show');`

Nos vamos a la siguiente ruta y crearemos un nuevo archivo que se llamara  `PostsController` el cual contendra las rutas de test

![coamdo tree](21.jpg "comando tree")




# Laravel From The Scratch - 3. Database Access
## Setup a Database Connection

Hasta ahora, hemos estado usando una matriz simple como nuestro almacén de datos. Esto no es muy realista, así que aprendamos a configurar una conexión de base de datos. En este episodio, analizaremos las variables de entorno, los archivos de configuración y el generador de consultas.


## Configure una conexión de base de datos

en esta parte del corsu vamos a configurar la base de datos para eso vamos a ir al archivo de .env

![coamdo tree](22.jpg "comando tree")

Cuando ya lo dentangomos abierto iremos a esta parte de los datos 

![coamdo tree](23.jpg "comando tree")

Está parte muestra
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=blog.isw811.xyz
    DB_USERNAME=root
    DB_PASSWORD=

ahora vamos a configurar lo que seria el nombre de la base de datos el cual nosotros tenemos la que es  ` blog.isw811.xyz`   

![coamdo tree](24.jpg "comando tree")


De esta forma muestra los datos que tengamos de la base de datos 



    Public function show($slug)
    {
       
  $post = .\DB::table('posts')->where('slug', $slug)->first();
   dd($post);    
     return view('post',[
        'post' => $post 
     ]);
    }
}


 <h1>{!! $post->body !!}</h1>

## 10. Hello Eloquent

En el episodio anterior, usamos el generador de consultas para buscar la publicación relevante de la base de datos. Sin embargo, hay una segunda opción que debemos considerar: Eloquent. Una clase Eloquent no solo proporciona la misma API limpia para consultar su base de datos, sino que también es el lugar perfecto para almacenar cualquier lógica comercial adecuada.

Esta es otra forma de creae una conexion a base de datos 
colocar en use DB y donde esta $post le quitaremos .\ y ademas de eso controlaremos por si tiene algun error a la hora de conectarse a la base ded atos 


    use DB;

    Public function show($post)
    {
       
        $post = DB::table('posts')->where('slug', $slug)->first();
       
        if(!$post){
            abort(404);
        }


        
            return view('post',[
                'post' => $post 
            ]);
    }

Ahora crearemos un modelo el cual dentra el nombre de`Post.php`   

![coamdo tree](25.jpg "comando tree")

El cual se deberia de ver asi 

![coamdo tree](26.jpg "comando tree")

Luego de eso nos volveremos al controlador de Post donde colocaremos la extenxion hacia el modelo y modificaremos un poco el codigo

![coamdo tree](27.jpg "comando tree")


## 11. Migrations 101

En un episodio anterior, creamos manualmente una tabla de base de datos; sin embargo, esto no refleja el flujo de trabajo típico que seguirá en su codificación diaria. En su lugar, lo más habitual es buscar clases de migración. En este episodio, discutiremos qué son y por qué son útiles.


Para eso nos tendremos que ir donde esta las migrations en el poryecto asi que siguimos la siguiente ruta y escogemos el archivo marcado.

![coamdo tree](28.jpg "comando tree")

Asi se tiene que ver el documento

![coamdo tree](29.jpg "comando tree")

Ahora vamos a modificar un poco el documento y la agregamos lo siguiente lo cuales seran los campos de la tabla


![coamdo tree](30.jpg "comando tree")


# Laravel From The Scratch - 4. Views
## 4. Layout Pages

Si revisa la vista de bienvenida que viene con Laravel, contiene la estructura HTML completa hasta el doctype. Esto está bien para una página de demostración, pero en la vida real, en su lugar, buscará archivos de diseño.


El layout que crearemos es para facilitarnos las cosas ybno estar copiando y pegando diseño en otras paginas, asi que con este layout pondremos el diseño que mas se repite  mas y se podra utilizar en otras paginas, tambien se puede hacer con otras cosas que se repiten mucho como el contenido
![coamdo tree](31.jpg "comando tree")
 
simple colocamos en la pagina que queramos utilizar el layout lo siguiente `@extends('layout') `  



## 15. Integrate a Site Template

Usando las técnicas que ha aprendido en los últimos episodios, integremos una plantilla de sitio gratuita en nuestro proyecto Laravel, llamado SimpleWork.

se utliza una plantilla ya echa la cual se modifica 

## 16.Establezca un enlace de menú activo

Se pueden colocar un modo de navegacion en el menu para que lo lleve a una pagina especifica 
![coamdo tree](32.jpg "comando tree")

Esta manera muestra como colocar para que cada vinculo lo lleva a diferentes paginas.
![coamdo tree](33.jpg "comando tree")


## 17. Compilación de activos con Laravel Mix y Webpack
Laravel proporciona una herramienta útil llamada Mix, un envoltorio de paquete web, para ayudar con la agrupación y compilación de activos. 



![coamdo tree](34.jpg "comando tree")
![coamdo tree](35.jpg "comando tree")
![coamdo tree](36.jpg "comando tree")



## 18.Render Dynamic Data

A continuación, aprendamos a representar datos dinámicos. La página "acerca de" de la plantilla de sitio que estamos usando contiene una lista de artículos. Creemos un modelo para estos, almacenemos algunos registros en la base de datos y luego los rendericemos dinámicamente en la página.

![coamdo tree](37.jpg "comando tree")
![coamdo tree](38.jpg "comando tree")


# Laravel From The Scratch - 5. Forms
## 21.The Seven Restful Controller Actions

tenemos varios diferentes de action como el create,update, edit, view, delete,edite, destroy, store

## 22 Enrutamiento relajante
asi son las maneras para enrutar y hacer que vaya hacer un create,update, edit, view, delete,edite, destroy, store de la fomra correcta
![coamdo tree](39.jpg "comando tree")

Asi es como se crea un create uno lo manda al fomulario y en el fomr en el method se coloca que sera un POST y en el action se colocara la ruta 
![coamdo tree](40.jpg "comando tree")

en este se hara un redirociamiento hacia create en el cual se encargara a guardar los datos en la base de datos
![coamdo tree](41.jpg "comando tree")

## 24. Formularios que envían solicitudes PUT
El PUT se utiliza para editar los datos , en este caso lo haremos de esta manera ya que normalmente los navegadores solo aceptan lo que seria el GET y POST

![coamdo tree](42.jpg "comando tree")
![coamdo tree](43.jpg "comando tree")
![coamdo tree](44.jpg "comando tree")
![coamdo tree](45.jpg "comando tree")

## 25. Fundamentos de la validación de formularios
Hay varias formas de validar un formulario para permitir que ciertos datos puedan ser insertados o modificados las formas son las siguiente
Mediante codigo seria de esta manera
![coamdo tree](46.jpg "comando tree")
y la otra seria mediante una propiedad que el mismo input tiene que seria required
![coamdo tree](47.jpg "comando tree")

